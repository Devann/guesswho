# Guess Who Web Game

## Introduction

Welcome to the Guess Who web game – a digital adaptation of the classic board game. This project recreates the engaging experience of Guess Who through a web application. It features an Angular-based frontend for the user interface and Node.js for the backend. The entire application has been containerized using Docker for seamless deployment and management.

## Features

- Interactive web-based version of the Guess Who game.
- Frontend developed with Angular, offering a modern and engaging user interface.
- Backend powered by Node.js for efficient game logic and data management.
- Docker containerization for easy deployment and scalability.

## Author

- Devan PRIGENT

## Prerequisites

- Node.js and npm (for the frontend and backend)
- Angular CLI
- Docker

## Getting Started

1. Clone this repository.
2. Execute the `script.ps1` to create the docker image and launch the container automatically.
3. Access the application through your browser at `http://localhost:4200`.
