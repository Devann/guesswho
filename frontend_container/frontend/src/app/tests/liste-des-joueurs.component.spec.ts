import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDesJoueursComponent } from '../liste-des-joueurs/liste-des-joueurs.component';

describe('ListeDesJoueursComponent', () => {
  let component: ListeDesJoueursComponent;
  let fixture: ComponentFixture<ListeDesJoueursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeDesJoueursComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListeDesJoueursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
