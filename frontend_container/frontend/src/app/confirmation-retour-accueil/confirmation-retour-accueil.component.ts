import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-retour-accueil',
  templateUrl: './confirmation-retour-accueil.component.html',
  styleUrls: ['./confirmation-retour-accueil.component.css']
})
export class ConfirmationRetourAccueilComponent {

  constructor(public dialogRef: MatDialogRef<ConfirmationRetourAccueilComponent>) {}

}
