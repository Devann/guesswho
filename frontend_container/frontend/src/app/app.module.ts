import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './services/app-routing.module';
import { AccueilComponent } from './accueil/accueil.component';
import { HeaderComponent } from './header/header.component';
import { ChoixCarteComponent } from './choix-carte/choix-carte.component';
import { TableauDeJeuComponent } from './tableau-de-jeu/tableau-de-jeu.component';
import { SectionCarteMystereComponent } from './section-carte-mystere/section-carte-mystere.component';
import { EcranDeFinComponent } from './ecran-de-fin/ecran-de-fin.component';
import { SectionJeuComponent } from './section-jeu/section-jeu.component';
import { ChargementComponent } from './chargement/chargement.component';
import { ListeDesJoueursComponent } from './liste-des-joueurs/liste-des-joueurs.component';
import { ConfirmationRetourAccueilComponent } from './confirmation-retour-accueil/confirmation-retour-accueil.component';
import { ConfirmationInvitationComponent } from './confirmation-invitation/confirmation-invitation.component';
import { ConfigurationPartieComponent } from './configuration-partie/configuration-partie.component';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    HeaderComponent,
    ChoixCarteComponent,
    TableauDeJeuComponent,
    SectionCarteMystereComponent,
    EcranDeFinComponent,
    SectionJeuComponent,
    ChargementComponent,
    ListeDesJoueursComponent,
    ConfirmationRetourAccueilComponent,
    ConfirmationInvitationComponent,
    ConfigurationPartieComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatGridListModule,
    AppRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
