import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';

@Component({
  selector: 'app-configuration-partie',
  templateUrl: './configuration-partie.component.html',
  styleUrls: ['./configuration-partie.component.css']
})
export class ConfigurationPartieComponent implements OnInit {

  private socket?: any;
  public usernamePerso: string;
  public usernameAdverse: string;
  public checkedFR: boolean = false;
  public checkedTSP: boolean = false;
  public checkedFIPA: boolean = false;
  public effectif : Number[];
  public admin: boolean;

  qcm = this.formBuilder.group({
    frerots: false,
    tsp: false,
    fipa: false,
  });

  constructor(private router: Router, private formBuilder: FormBuilder, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  public ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
    this.usernameAdverse = this.gestionnaireCarte.getUsernameAdverse();
    this.usernamePerso = this.gestionnaireCarte.getUsernamePerso();
    this.effectif = this.gestionnaireCarte.getTaillesPaquets();
    this.admin = this.gestionnaireCarte.getAdmin();
  }

  public start() {
    this.gestionnaireCarte.setCtrl(this.qcm.value);
    let taillePaquet = this.gestionnaireCarte.getTaillePaquet();
    this.socket.emit("choix_config", this.qcm.value, taillePaquet);
  }

  public ngAfterViewInit() {

    this.socket.on("deconnexion", (msg) => {
      this.gestionnaireCarte.setMessage(msg);
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("connect", () => {
      this.gestionnaireCarte.setMessage("Vous avez été déconnecté, vous allez être redirigé vers l'écran d'accueil");
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("choix-carte", (config) => {
      this.gestionnaireCarte.setCtrl(config);
      this.router.navigateByUrl('/choix-carte');
    })
  }
}
