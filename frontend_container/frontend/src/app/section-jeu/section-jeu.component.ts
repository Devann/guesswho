import { Component, OnInit } from '@angular/core';
import { Carte } from '../data/carte';
import { Router } from '@angular/router';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';

@Component({
  selector: 'app-section-jeu',
  templateUrl: './section-jeu.component.html',
  styleUrls: ['./section-jeu.component.css']
})

export class SectionJeuComponent implements OnInit {

  socket?: any;
  cartes?: Carte[];
  affichageProposition: number = 0;
  texteBouton: string = "Proposer une réponse";

  constructor(private router: Router, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
    this.cartes = this.gestionnaireCarte.getjeuCartes();

    for (let i = 0; i < this.cartes.length; i++) {
      this.cartes[i].etat = true;
    }
  }

  public proposition(carte: Carte): void {
    this.socket.emit("proposition_carte", carte);
  }

  public ngAfterViewInit() {
    
    this.socket.on("deconnexion", (msg) => {
      this.gestionnaireCarte.setMessage(msg);
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("connect", () => {
      this.gestionnaireCarte.setMessage("Vous avez été déconnecté, vous allez être redirigé vers l'écran d'accueil");
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("resultat", (resultat,id) => {
      this.gestionnaireCarte.setCarteAdverse(id);
      if (resultat === "victoire") {
        this.gestionnaireCarte.setResultat(true);
      }
      else {
        this.gestionnaireCarte.setResultat(false);
      }
      this.router.navigateByUrl('/ecran-fin');
    })
  }
}
