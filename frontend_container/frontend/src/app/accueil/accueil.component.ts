import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  public username: string;
  private socket?: any;

  constructor(private router: Router, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
  }

  public start() {
    if (this.username) {
      this.gestionnaireCarte.setUsernamePerso(this.username);
      this.socket.emit("username", this.username);
      this.router.navigateByUrl('/liste-joueurs');
    }
  }

}
