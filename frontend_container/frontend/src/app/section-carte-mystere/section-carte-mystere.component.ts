import { Component, OnInit } from '@angular/core';
import { Carte } from '../data/carte';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';

@Component({
  selector: 'app-section-carte-mystere',
  templateUrl: './section-carte-mystere.component.html',
  styleUrls: ['./section-carte-mystere.component.css']
})
export class SectionCarteMystereComponent implements OnInit {

  public carte ?: Carte ;
  public adversaire ?: string;
  
  constructor(private gestionnaireCarte : GestionnaireCarteService) { }

  ngOnInit(): void {
    this.carte = this.gestionnaireCarte.getCardPerso();
    this.adversaire = this.gestionnaireCarte.getUsernameAdverse();
  }

}
