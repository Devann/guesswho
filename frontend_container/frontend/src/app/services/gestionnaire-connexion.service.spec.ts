import { TestBed } from '@angular/core/testing';

import { GestionnaireConnexionService } from './gestionnaire-connexion.service';

describe('GestionnaireConnexionService', () => {
  let service: GestionnaireConnexionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionnaireConnexionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
