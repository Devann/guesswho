import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';

@Component({
  selector: 'app-confirmation-invitation',
  templateUrl: './confirmation-invitation.component.html',
  styleUrls: ['./confirmation-invitation.component.css']
})
export class ConfirmationInvitationComponent {

  public adversaire;

  constructor(public dialogRef: MatDialogRef<ConfirmationInvitationComponent>, private gestionnaireCarte: GestionnaireCarteService) {}

  ngOnInit(): void {
    this.adversaire = this.gestionnaireCarte.getAdversaire();
  }

}
