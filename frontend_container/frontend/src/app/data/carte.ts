export interface Carte {
    id: number;
    nom: string;
    prenom: string;
    etat: boolean;
    url: string;
  }