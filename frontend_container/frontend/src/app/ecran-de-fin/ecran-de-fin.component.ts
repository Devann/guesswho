import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';

@Component({
  selector: 'app-ecran-de-fin',
  templateUrl: './ecran-de-fin.component.html',
  styleUrls: ['./ecran-de-fin.component.css']
})
export class EcranDeFinComponent implements OnInit {

  socket?: any;
  message?: string;
  etat?: string;
  urlImageAdverse = '/assets/error_image.png';

  constructor(private router: Router, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
    let carteAdverse = this.gestionnaireCarte.getCarteAdverse();
    this.urlImageAdverse = carteAdverse.url;
    let resultat : boolean = this.gestionnaireCarte.getResultat();

    if (resultat) {
      this.message = "Victoire";
      this.etat = "success";
    }
    else {
      this.message = "Défaite";
      this.etat = "danger";
    }

  }

  public ngAfterViewInit() {

    this.socket.on("connect", () => {
      this.gestionnaireCarte.setMessage("Vous avez été déconnecté, vous allez être redirigé vers l'écran d'accueil");
      this.router.navigateByUrl('/chargement');
    })

  }
}

