import { Component, OnInit } from '@angular/core';
import { Carte } from '../data/carte';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choix-carte',
  templateUrl: './choix-carte.component.html',
  styleUrls: ['./choix-carte.component.css']
})
export class ChoixCarteComponent implements OnInit {

  public cartes?: Carte[];
  private socket?: any;
  public adversaire: string;

  constructor(private router: Router, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  public ngOnInit(): void {
    this.adversaire = this.gestionnaireCarte.getUsernameAdverse();
    this.socket = this.gestionnaireConnexion.getConnexion();
  }

  public selection(carte: Carte): void {
    this.gestionnaireCarte.setCardPerso(carte);
    this.socket.emit("selection_carte", carte);
    this.gestionnaireCarte.setMessage("Votre adversaire sélectionne sa carte");
    this.router.navigateByUrl('/chargement');
  }

  public ngAfterViewInit() {

    this.socket.on("deconnexion", (msg) => {
      this.gestionnaireCarte.setMessage(msg);
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("connect", () => {
      this.gestionnaireCarte.setMessage("Vous avez été déconnecté, vous allez être redirigé vers l'écran d'accueil");
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("paquet", (tirage) => {
      let paquet = this.gestionnaireCarte.getPaquet();
      let jeuCartes : Carte[] = [];
      for (let i = 0; i < tirage.length; i++) {
        jeuCartes.push(paquet[tirage[i]]);
      }
      this.cartes = jeuCartes;
      this.gestionnaireCarte.setjeuCartes(jeuCartes);
    })
  }

}
