import { Injectable } from '@angular/core';
import { io } from "socket.io-client";


@Injectable({
  providedIn: 'root'
})
export class GestionnaireConnexionService {

  private socket ;

  constructor() { 
    this.socket = io("http://localhost:3000");
  }

  public getConnexion(){
    return this.socket;
  }

}
