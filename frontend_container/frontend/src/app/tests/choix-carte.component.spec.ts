import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoixCarteComponent } from '../choix-carte/choix-carte.component';

describe('ChoixCarteComponent', () => {
  let component: ChoixCarteComponent;
  let fixture: ComponentFixture<ChoixCarteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoixCarteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChoixCarteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
