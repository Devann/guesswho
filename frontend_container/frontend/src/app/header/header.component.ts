import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Router } from '@angular/router';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';
import { ConfirmationRetourAccueilComponent } from '../confirmation-retour-accueil/confirmation-retour-accueil.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  private socket?: any;

  constructor(private router: Router, public dialog: MatDialog, private gestionnaireConnexion: GestionnaireConnexionService) { }

  ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
  }

  openDialog(): void {
    let dialogue = this.dialog.open(ConfirmationRetourAccueilComponent, {
      height: '130px',
      width: '670px',
    });

    dialogue.afterClosed().subscribe(result => {
      if (result) {
        this.socket.emit("accueil");
        this.router.navigateByUrl('/accueil');
      }
    });
  }
}
