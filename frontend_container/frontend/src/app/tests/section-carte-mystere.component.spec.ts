import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionCarteMystereComponent } from '../section-carte-mystere/section-carte-mystere.component';

describe('SectionCarteMystereComponent', () => {
  let component: SectionCarteMystereComponent;
  let fixture: ComponentFixture<SectionCarteMystereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionCarteMystereComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SectionCarteMystereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
