import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationInvitationComponent } from './confirmation-invitation.component';

describe('ConfirmationInvitationComponent', () => {
  let component: ConfirmationInvitationComponent;
  let fixture: ComponentFixture<ConfirmationInvitationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmationInvitationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfirmationInvitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
