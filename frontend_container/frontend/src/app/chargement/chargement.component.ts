import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';

@Component({
  selector: 'app-chargement',
  templateUrl: './chargement.component.html',
  styleUrls: ['./chargement.component.css']
})

export class ChargementComponent implements OnInit {

  socket?: any;
  message: string;

  constructor(private router: Router, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
    this.message = this.gestionnaireCarte.getMessage();

    if (this.message.indexOf("vous allez être redirigé vers l'écran d'accueil") !== -1) {
      (async () => {
        await new Promise(f => setTimeout(f, 3000));
        this.router.navigateByUrl('/accueil');
      })();
    }
  }

  public ngAfterViewInit() {

    this.socket.on("deconnexion", (msg) => {
      this.gestionnaireCarte.setMessage(msg);
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("connect", () => {
      this.router.navigateByUrl('/accueil');
    })
    
    this.socket.on("lancement_du_jeu", () => {
      this.router.navigateByUrl('/tableau-de-jeu');
    })
  }
}
