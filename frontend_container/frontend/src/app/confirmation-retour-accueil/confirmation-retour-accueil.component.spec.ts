import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationRetourAccueilComponent } from './confirmation-retour-accueil.component';

describe('ConfirmationRetourAccueilComponent', () => {
  let component: ConfirmationRetourAccueilComponent;
  let fixture: ComponentFixture<ConfirmationRetourAccueilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmationRetourAccueilComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfirmationRetourAccueilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
