import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationPartieComponent } from './configuration-partie.component';

describe('ConfigurationPartieComponent', () => {
  let component: ConfigurationPartieComponent;
  let fixture: ComponentFixture<ConfigurationPartieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationPartieComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfigurationPartieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
