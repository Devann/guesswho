// MODULES -----------------------------------------------------
const app = require("express")();
const http = require("http").Server(app);
const colors = require('colors');
const io = require("socket.io")(http, {
    cors: {
        origin: "*",
        credentials: true
    }
});



// FONCTIONS ET CLASSES ----------------------------------------
class User {
    constructor(username, socket, carteChoisie, partie, enJeu) {
        this.username = username;
        this.socket = socket;
        this.carteChoisie = carteChoisie;
        this.partie = partie;
        this.enJeu = false;
    };
}

class Partie {
    constructor(id, user1, user2) {
        this.id = id;
        this.user1 = user1;
        this.user2 = user2;

        user1.partie = this;
        user2.partie = this;
        user1.socket.join(numRoom);
        user2.socket.join(numRoom);
    }

    getOtherUser(user) {
        if (user === this.user1) {
            return this.user2;
        }
        else {
            return this.user1;
        }
    }

    ejection() {
        deleteUser(this.user1);
        deleteUser(this.user2);
    }
}

function deleteUser(user) {
    let index = users.indexOf(user);
    users.splice(index, 1);
    io.emit("liste-joueur", listeUtilisateurs(users));
}

function listeUtilisateurs(users) {
    let liste = [];
    for (var i = 0; i < users.length; i++) {
        if (!users[i].enJeu) {
            liste.push({
                "username": users[i].username,
                "id": users[i].socket.id,
                "inviter":false
            });
        }
    }
    return liste;
}

function logListeUtilisateurs() {
    let liste = [];
    for (var i = 0; i < users.length; i++) {
        liste.push(users[i].username);
    }
    console.log(`[INFO] Liste des utilisateurs = (${liste})`.blue);
}

function majUtilisateurs(io) {
    io.emit("liste-joueur", listeUtilisateurs(users));
    logListeUtilisateurs();
}

function deconnexion(socket) {
    console.log(`[8] Déconnexion utilisateur (${socket.id})`.green);

    // On supprime l'utilisateur
    let user = users.find(user => user.socket === socket);
    if (user !== undefined) {
        deleteUser(user);
        if (user.partie !== null) {
            // S'il existe, on supprime son adversaire
            let user2 = user.partie.getOtherUser(user);
            deleteUser(user2);
            // On ejecte son adversaire de la partie
            let room = socket.rooms.values();
            let sid = room.next().value;
            let roomname = room.next().value;
            socket.to(roomname).emit("deconnexion", "Votre adversaire a été déconnecté, vous allez être redirigé vers l'écran d'accueil");
        }
    }
    majUtilisateurs(io);
}


function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return (Math.floor(Math.random() * (max - min)) + min);
}


function getJeu(taille_paquet) {
    console.log(`taille_paquet : ${taille_paquet}`);
    let tirage = [];
    console.log(Math.min(24, taille_paquet));
    for (let i = 0; i < Math.min(24, taille_paquet); i++) {
        let int = getRandom(0, taille_paquet);
        while (tirage.includes(int)) {
            int = getRandom(0, taille_paquet);
            console.log(int);
        }
        tirage.push(int);
    }
    console.log(`Tirage : ${tirage}`);
    return tirage;
}



// VARIABLES ---------------------------------------------------
var users = [];
var numRoom = 0;



// MAIN --------------------------------------------------------
io.on("connection", socket => {
    console.log(`[1] Connexion utilisateur (${socket.id})`.green);


    socket.on("accueil", function () {
        deconnexion(socket);
    })


    socket.on("username", function (username) {
        console.log(`[2] Authentification utilisateur (${socket.id} = ${username})`.green);
        let user = new User(username, socket, null, null);
        users.push(user);
        majUtilisateurs(io);
    });


    socket.on("inviter", function (joueur) {
        let user1 = users.find(user => user.socket === socket);
        let user2 = users.find(user => user.socket.id === joueur.id);
        user2.socket.emit("invitation", {
            "username": user1.username,
            "id": user1.socket.id
        });
        console.log(`[3] Invitation (${user1.username} -> ${user2.username})`.green);
    })


    socket.on("invitation_acceptee", function (joueur) {
        console.log(`[4] L'invitation a été acceptée`.green);
        let user1 = users.find(user => user.socket === socket);
        let user2 = users.find(user => user.socket.id === joueur.id);
        new Partie(numRoom, user1, user2);
        user1.enJeu = true;
        user2.enJeu = true;
        majUtilisateurs(io);

        console.log(`[5] Début de partie`.green)
        user1.socket.emit("donneesInitiales", user2.username);
        user2.socket.emit("donneesInitiales", user1.username);
        io.in(numRoom).emit("configuration");
        numRoom += 1;
    });


    socket.on("choix_config", function (config, taille_paquet) {
        let user = users.find(user => user.socket === socket);
        let numRoom = user.partie.id;
        io.in(numRoom).emit("choix-carte", config);

        let tirage = getJeu(taille_paquet);
        console.log(`[6] Tirage effectué`.green);
        io.in(numRoom).emit("paquet", tirage);
    });


    socket.on("selection_carte", function (carteChoisie) {
        let user1 = users.find(user => user.socket === socket);
        user1.carteChoisie = carteChoisie;
        let user2 = user1.partie.getOtherUser(user1);
        if (user2.carteChoisie != null) {
            user1.socket.emit("lancement_du_jeu");
            user2.socket.emit("lancement_du_jeu");
        }
    });


    socket.on("proposition_carte", function (carte) {
        console.log(`[7] Fin de la partie`.green);
        let emetteur = users.find(user => user.socket === socket);
        let partie = emetteur.partie;
        recepteur = partie.getOtherUser(emetteur);
        try {
            if (recepteur.carteChoisie.id === carte.id) {
                emetteur.socket.emit("resultat", "victoire", recepteur.carteChoisie.id);
                recepteur.socket.emit("resultat", "defaite", emetteur.carteChoisie.id);
            }
            else {
                emetteur.socket.emit("resultat", "defaite", recepteur.carteChoisie.id);
                recepteur.socket.emit("resultat", "victoire", emetteur.carteChoisie.id);
            }
            partie.ejection();
            majUtilisateurs(io);
        }
        catch (e) {
            console.log(`[ERR] Erreur attrapée`.red);
            for (let i = 0; i < numRoom; i++) {
                console.log(`[ERR] Déconnexion de la room ${i}`.red);
                io.in(i).emit("deconnexion", "L'application a rencontré un problème, vous allez être redirigé vers l'écran d'accueil");
            }
            console.log(e);
        };
    });


    socket.on("disconnecting", () => {
        deconnexion(socket);
    });
});


http.listen(3000, () => {
    console.log(`[INFO] Lancement du serveur sur le port 3000`.blue);
});