import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauDeJeuComponent } from '../tableau-de-jeu/tableau-de-jeu.component';

describe('TableauDeJeuComponent', () => {
  let component: TableauDeJeuComponent;
  let fixture: ComponentFixture<TableauDeJeuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauDeJeuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TableauDeJeuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
