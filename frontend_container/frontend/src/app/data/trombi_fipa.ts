import { Carte } from './carte';

export const fipa: Carte[] = [
    { id: 11, nom: 'GERENTON', prenom: 'Victor', etat: true, url: '/assets/fipa/1.jpeg' },
    { id: 12, nom: 'DA SILVA', prenom: 'Simon', etat: true, url: '/assets/fipa/2.jpeg' },
    { id: 13, nom: 'LANDRAIN', prenom: 'Anthony', etat: true, url: '/assets/fipa/3.jpeg' }
]