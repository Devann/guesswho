import { Carte } from './carte';

export const frerots: Carte[] = [
    { id: 1, nom: 'BLANCHONNET', prenom: 'Théo', etat: true, url: '/assets/frerots/1.jpeg' },
    { id: 2, nom: 'CAILLARD', prenom: 'Mathias', etat: true, url: '/assets/frerots/2.jpeg' },
    { id: 3, nom: 'JACQUET', prenom: 'Marin', etat: true, url: '/assets/frerots/3.jpeg' },
    { id: 4, nom: 'LEGOUIC', prenom: 'Nicolas', etat: true, url: '/assets/frerots/4.jpeg' },
    { id: 5, nom: 'MARIGNIER', prenom: 'Louis', etat: true, url: '/assets/frerots/5.jpeg' },
    { id: 6, nom: 'MORRISSEY', prenom: 'Charlie', etat: true, url: '/assets/frerots/6.jpeg' },
    { id: 7, nom: 'PRIGENT', prenom: 'Devan', etat: true, url: '/assets/frerots/7.jpeg' },
    { id: 8, nom: 'ROBINE', prenom: 'Thibault', etat: true, url: '/assets/frerots/8.jpeg' },
    { id: 9, nom: 'ROUSSELOT', prenom: 'Thomas', etat: true, url: '/assets/frerots/9.jpeg' },
    { id: 10, nom: 'VIGNON', prenom: 'Thomas', etat: true, url: '/assets/frerots/10.jpeg' },
    { id: 11, nom: 'GERENTON', prenom: 'Victor', etat: true, url: '/assets/frerots/11.jpeg' },
    { id: 12, nom: 'DA SILVA', prenom: 'Simon', etat: true, url: '/assets/frerots/12.jpeg' },
    { id: 13, nom: 'LANDRAIN', prenom: 'Anthony', etat: true, url: '/assets/frerots/13.jpeg' }
]