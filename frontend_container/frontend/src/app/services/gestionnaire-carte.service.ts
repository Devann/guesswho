import { Injectable } from '@angular/core';
import { Carte } from '../data/carte';
import { frerots } from '../data/trombi_frerots';
import { fipa } from '../data/trombi_fipa';
import { tsp } from '../data/trombi_tsp';

@Injectable({
  providedIn: 'root'
})
export class GestionnaireCarteService {

  private jeuCartes: Carte[] = [];
  private usernamePerso: string;
  private usernameAdverse: string;

  private ctrlFrerots: boolean = false;
  private ctrlTsp: boolean = false;
  private ctrlFipa: boolean = false;

  private admin: boolean = true;
  private carteAdverse;
  private adversaire;
  private cartePerso?: Carte;
  private resultat: boolean;
  private messageChargement: string = "";

  constructor() { }

  public setCtrl(qcm) {
    this.ctrlFrerots = qcm.frerots;
    this.ctrlTsp = qcm.tsp;
    this.ctrlFipa = qcm.fipa;
  }

  public getPaquet() {
    let paquet = [];
    if (this.ctrlTsp) {
      paquet = paquet.concat(tsp);
    }
    if (this.ctrlFrerots) {
      paquet = paquet.concat(frerots);
    }
    if (this.ctrlFipa){
      paquet = paquet.concat(fipa);
    }
    return paquet;
  }

  public setAdmin(valeur): void {
    this.admin = valeur;
  }

  public getAdmin(): boolean {
    return this.admin;
  }

  public getTaillePaquet(): Number{
    let paquet = this.getPaquet();
    let taille = paquet.length;
    return taille;
  }

  public getTaillesPaquets() {
    let t1 = frerots.length;
    let t2 = tsp.length;
    let t3 = fipa.length;
    return [t1, t2, t3];
  }

  public setjeuCartes(jeuCartes) {
    this.jeuCartes = jeuCartes;
  }

  public getjeuCartes() {
    return this.jeuCartes;
  }

  public setUsernamePerso(username: string) {
    this.usernamePerso = username;
  }

  public getUsernamePerso(): string {
    return this.usernamePerso;
  }

  public setUsernameAdverse(username: string) {
    this.usernameAdverse = username;
  }

  public getUsernameAdverse(): string {
    return this.usernameAdverse;
  }

  public setCarteAdverse(id) {
    let paquet = this.getPaquet();
    let carteAdverse = paquet.find(carte => carte.id === id);
    this.carteAdverse = carteAdverse;
  }

  public getCarteAdverse() {
    return this.carteAdverse;
  }

  public setAdversaire(adversaire) {
    this.adversaire = adversaire;
  }

  public getAdversaire() {
    return this.adversaire;
  }

  public setCardPerso(carte: Carte) {
    this.cartePerso = carte;
  }

  public getCardPerso(): Carte {
    return this.cartePerso;
  }

  public setResultat(resultat: boolean) {
    this.resultat = resultat;
  }

  public getResultat(): boolean {
    return this.resultat;
  }

  public setMessage(message: string) {
    this.messageChargement = message;
  }

  public getMessage(): string {
    return this.messageChargement;
  }
}
