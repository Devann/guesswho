import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionJeuComponent } from '../section-jeu/section-jeu.component';

describe('SectionJeuComponent', () => {
  let component: SectionJeuComponent;
  let fixture: ComponentFixture<SectionJeuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionJeuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SectionJeuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
