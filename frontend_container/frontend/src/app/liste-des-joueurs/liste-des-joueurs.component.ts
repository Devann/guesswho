import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { GestionnaireCarteService } from '../services/gestionnaire-carte.service';
import { GestionnaireConnexionService } from '../services/gestionnaire-connexion.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationInvitationComponent } from '../confirmation-invitation/confirmation-invitation.component';
import {MatSnackBar} from '@angular/material/snack-bar';



@Component({
  selector: 'app-liste-des-joueurs',
  templateUrl: './liste-des-joueurs.component.html',
  styleUrls: ['./liste-des-joueurs.component.css']
})
export class ListeDesJoueursComponent implements OnInit {

  public username;
  public joueurs;
  private socket?: any;

  constructor(private snackBar: MatSnackBar, public dialog: MatDialog, private router: Router, private gestionnaireCarte: GestionnaireCarteService, private gestionnaireConnexion: GestionnaireConnexionService) { }

  ngOnInit(): void {
    this.socket = this.gestionnaireConnexion.getConnexion();
    this.username = this.gestionnaireCarte.getUsernamePerso();
  }

  public ngAfterViewInit() {
    this.socket.on("connect", () => {
      this.gestionnaireCarte.setMessage("Vous avez été déconnecté, vous allez être redirigé vers l'écran d'accueil");
      this.router.navigateByUrl('/chargement');
    })

    this.socket.on("liste-joueur", (users) => {
      let liste = [];
      for (let i = 0; i < users.length; i++) {
        if (users[i].id !== this.socket.id) {
          liste.push(users[i]);
        }
      }
      this.joueurs = liste;
    })

    this.socket.on("donneesInitiales", (usernameAdverse) => {
      this.gestionnaireCarte.setUsernameAdverse(usernameAdverse);
    })

    this.socket.on("configuration", () => {
      this.router.navigateByUrl('/configuration');
    })

    this.socket.on("invitation", (adversaire) => {
      this.gestionnaireCarte.setAdversaire(adversaire);

      let dialogue = this.dialog.open(ConfirmationInvitationComponent, {
        height: '160px',
        width: '430px',
      });

      dialogue.afterClosed().subscribe(result => {
        if (result) {
          this.gestionnaireCarte.setAdmin(false);
          this.socket.emit("invitation_acceptee", this.gestionnaireCarte.getAdversaire());
        }
      });
    })
  }

  public invitation(joueur) {
    this.socket.emit("inviter", joueur);
    joueur.inviter = true;
    let snack = this.snackBar.open("Invitation envoyée", "OK");

    snack.afterDismissed().subscribe(() => {
      console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      joueur.inviter = false;
      this.router.navigateByUrl('/liste-joueurs');
    });
  };
}
