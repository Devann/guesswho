import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from '../accueil/accueil.component';
import { EcranDeFinComponent} from '../ecran-de-fin/ecran-de-fin.component';
import { TableauDeJeuComponent } from '../tableau-de-jeu/tableau-de-jeu.component';
import { ChoixCarteComponent } from '../choix-carte/choix-carte.component';
import { ChargementComponent } from '../chargement/chargement.component';
import { ListeDesJoueursComponent } from '../liste-des-joueurs/liste-des-joueurs.component';
import { ConfigurationPartieComponent } from '../configuration-partie/configuration-partie.component';

const routes: Routes = [
  { path: '', redirectTo: '/accueil', pathMatch: 'full' },
  { path: 'accueil', component: AccueilComponent },
  { path: 'liste-joueurs', component:ListeDesJoueursComponent},
  { path: 'configuration', component:ConfigurationPartieComponent},
  { path: 'choix-carte', component: ChoixCarteComponent},
  { path: 'chargement', component: ChargementComponent },
  { path: 'tableau-de-jeu', component: TableauDeJeuComponent },
  { path: 'ecran-fin', component: EcranDeFinComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],  // configure le routeur avec les routes
  exports: [RouterModule]                // exporte le routeur pour le rendre accessible
})
export class AppRoutingModule { }