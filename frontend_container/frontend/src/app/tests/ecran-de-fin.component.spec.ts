import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcranDeFinComponent } from '../ecran-de-fin/ecran-de-fin.component';

describe('EcranDeFinComponent', () => {
  let component: EcranDeFinComponent;
  let fixture: ComponentFixture<EcranDeFinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcranDeFinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EcranDeFinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
