import { TestBed } from '@angular/core/testing';

import { GestionnaireCarteService } from './gestionnaire-carte.service';

describe('GestionnaireCarteService', () => {
  let service: GestionnaireCarteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionnaireCarteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
